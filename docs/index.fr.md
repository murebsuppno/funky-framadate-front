# Framadate - funky version

FR: Un logiciel libre de sondage fait par les contributeurs de l'association Framasoft, avec une API backend.

## Index

* Meeting notes
* Getting Started (yarn start / npm start)
* How to contribute
* Architecture
* Translation i18n
* Accesibility
* Licence GNU affero V3

# Présentation
FR: Un logiciel libre de sondage fait par les contributeurs de l'association Framasoft, avec une API de backend en Symfony 5.

## Pour débuter
[lire la doc pour débuter votre Funky Framadate](GETTING_STARTED.md)

## Documentation
FR: Toute la documentation est disponible [dans le dossier "doc"](/), principalement en Français.
EN: All documentation is available in the "doc" folder, mainly in French because reasons.


# Version funky framadate

* [Spécifications](cadrage/specifications-fonctionnelles)
* maquettes par @maiwann : https://scene.zeplin.io/project/5d4d83d68866d6522ff2ff10
* vidéo de démo des maquettes par @maiwann : https://nuage.maiwann.net/s/JRRHTR9D2akMAa7
* discussions sur framateam canal général : https://framateam.org/ux-framatrucs/channaels/framadate
* discussions techniques côté développeurs : https://framateam.org/ux-framatrucs/channels/framadate-dev
* [notes de réunion](reunions/notes-de-reunion)
* [traductions](traductions)


# Documentations sur Angular

* `{- sur sass -}` (on va utiliser CSS, si angular permet d'avoir des variables CSS, @newick)


# Exemple de maquette de la nouvelle version

![schéma de données](img/date-poll-api-schema.png)
[voir le schéma UML](img/date-poll-api-schema-diagram.uml)

--
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.1.
For full documentation on mkdocs visit [mkdocs.org](https://www.mkdocs.org).
notes-de-reunion)
* [traductions](traductions)


# Documentations sur Angular

* `{- sur sass -}` (on va utiliser CSS, si angular permet d'avoir des variables CSS, @newick)

# Exemple de maquette de la nouvelle version

![funky_framadate_maquette](img/framadate_funky_design.png)


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.1.
For full documentation on mkdocs visit [mkdocs.org](https://www.mkdocs.org).
