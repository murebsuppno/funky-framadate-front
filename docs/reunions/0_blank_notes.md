Framadate suivi - (__date de réunion__)
========================================
###### tags: #framadate, #suivi

> Participants à la réunion:
* 
* 
* 

## État des lieux

### > Où on en est
https://framagit.org/framasoft/framadate/funky-framadate-front/-/boards
> Quel est le projet ? Quels sont les enjeux ?  
> Quelles méthodes de travail ?  
> Quel niveau de qualité ? standards et de bonnes pratiques ?
>   *
> Quelles priorités ?  
>   *
> Qui veut faire quoi ?  
>   * 

## Notes de réunion
- 

## Trucs à faire
- 

## Décisions prises
- 

## Ressources

* Discussion : [https://framateam.org/ux-framatrucs/channels/framadate]()
* Repo front/dev : [https://framagit.org/framasoft/framadate/funky-framadate-front/tree/dev]()
* Repo back : [https://framagit.org/framasoft/framadate/framadate ]()
* Maquettes Zeplin : demander l'accès à maiwann
* La démo : [https://framadate-api.cipherbliss.com/]()
* Vidéo de présentation : [https://nuage.maiwann.net/s/JRRHTR9D2akMAa7]()
