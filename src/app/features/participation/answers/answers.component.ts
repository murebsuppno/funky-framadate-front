import { Component, Input, OnInit } from '@angular/core';

import { Poll } from '../../../core/models/poll.model';
import { Owner } from '../../../core/models/owner.model';

@Component({
	selector: 'app-answers',
	templateUrl: './answers.component.html',
	styleUrls: ['./answers.component.scss'],
})
export class AnswersComponent implements OnInit {
	@Input() poll: Poll;
	@Input() user: Owner;

	constructor() {}

	ngOnInit(): void {}
}
