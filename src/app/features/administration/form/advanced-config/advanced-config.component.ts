import { Component, Input, OnInit } from '@angular/core';
import { Poll } from '../../../../core/models/poll.model';
import { UntypedFormGroup } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { PollService } from '../../../../core/services/poll.service';

@Component({
	selector: 'app-advanced-config',
	templateUrl: './advanced-config.component.html',
	styleUrls: ['./advanced-config.component.scss'],
})
export class AdvancedConfigComponent implements OnInit {
	public urlPrefix = '/participation/';
	public environment = environment;
	public displayClearPassword = false;
	@Input()
	public poll?: Poll;
	@Input()
	public form: UntypedFormGroup;
	domain_url: string;
	display_regen_slug: boolean = environment.display_regen_slug;
	display_password_clear_button: boolean = environment.display_password_clear_button;
	constructor(public pollService: PollService) {}

	ngOnInit(): void {
		this.domain_url = 'https://' + new URL(window.location.href).hostname + '/';

		this.form.patchValue({ custom_url: this.pollService.makeSlug(this.pollService.form) });
	}
}
