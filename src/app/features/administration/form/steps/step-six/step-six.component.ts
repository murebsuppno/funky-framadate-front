import { Component, OnInit } from '@angular/core';
import { PollService } from '../../../../../core/services/poll.service';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-step-six',
	templateUrl: './step-six.component.html',
	styleUrls: ['./step-six.component.scss'],
})
export class StepSixComponent implements OnInit {
	public environment = environment;
	constructor(public pollService: PollService) {
		this.pollService.step_current = 6;
	}

	ngOnInit(): void {}
}
