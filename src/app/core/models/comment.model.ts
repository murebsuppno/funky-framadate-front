export interface CommentDTO {
	pseudo: string;
	text: string;
}
export class Comment {
	constructor(public id: string, public text: string, public pseudo: string, public created_at: string) {}

	public static sortChronologically(first: Comment, second: Comment): number {
		const a = new Date(first.created_at);
		const b = new Date(second.created_at);

		if (a < b) {
			return -1;
		}
		if (a > b) {
			return 1;
		}
		return 0;
	}
}
