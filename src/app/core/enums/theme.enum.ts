export enum Theme {
	LIGHT = 'LIGHT',
	DARK = 'DARK',
	CONTRAST = 'CONTRAST',
	RED = 'RED',
}
